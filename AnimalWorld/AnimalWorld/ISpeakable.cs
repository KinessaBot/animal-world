﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalWorld
{
    interface ISpeakable
    {
        void Speak();
    }
}
