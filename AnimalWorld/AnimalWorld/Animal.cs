﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalWorld
{
    class Animal
    {
        public string name { get; set; }

        public Animal(string name)
        {
            this.name = name;
        }
    }
}
