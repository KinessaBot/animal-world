﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalWorld
{
    class Group<T>: List<T> where T: ISpeakable, ISelfPresentable
    {
        string title;

        public Group(string title) : base()
        {
            this.title = title;
        }

        public void Noise()
        {
            IEnumerator<T> iter = base.GetEnumerator();
            while (iter.MoveNext())
                iter.Current.Speak();
        }

        public void Presentation()
        {
            IEnumerator<T> iter = base.GetEnumerator();
            while (iter.MoveNext())
                iter.Current.SelfPresent();
            Console.WriteLine("we are " + title);
        }

    }
}
