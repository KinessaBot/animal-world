﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalWorld
{
    class Dog : Animal, ISpeakable, ISelfPresentable
    {
        public void Speak()
        {
            Console.WriteLine(name + " goes Bark");
        }

        public Dog(string name) : base(name)
        { }

        public void SelfPresent()
        {
            Console.WriteLine(name + ". Nice to meet you");
        }
    }
}
