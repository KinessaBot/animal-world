﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace AnimalWorld
{
    class Program
    {
        static void Main()
        {
            Animal[] myAnimals = { new Cow("Betsy"), new Cow("Roberto"), new Dog("Steven"), new Cat("Alice")};
            Animal[] badAnimals = { new Cow("Bobby"), new Cow("Roberto"), new Cat("Steven"), new Cat("Alice") };

            IEnumerable<string> cows = from animal in myAnimals
                       where animal is Cow
                       select animal.name;

            IEnumerable<string> bads = from myAnimal in myAnimals
                                       from badAnimal in badAnimals
                                       where (badAnimal.name == myAnimal.name && badAnimal.GetType() == myAnimal.GetType())
                                       orderby myAnimal.name
                                       select myAnimal.name;

            var intersectionAnimals = from myAnimal in myAnimals
                                      join badAnimal in badAnimals on myAnimal.name equals badAnimal.name
                                      select myAnimal;

            foreach (string cow in cows)
                Console.WriteLine(cow);
            Console.WriteLine();
            foreach (string bad in bads)
                Console.WriteLine(bad);
            Console.WriteLine();
            foreach (Animal intersection in intersectionAnimals)
                Console.WriteLine(intersection.name);


            Console.ReadLine();
        }
    }
}
