﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalWorld
{
    class Cow : Animal, ISpeakable, ISelfPresentable
    {
        public void Speak()
        {
            Console.WriteLine(name + " goes Moo");
        }

        public Cow(string name) : base(name)
        { }

        public void SelfPresent()
        {
            Console.WriteLine("My name is " + name);
        }
    }
}
