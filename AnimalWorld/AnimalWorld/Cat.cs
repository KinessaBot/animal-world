﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnimalWorld
{
    class Cat : Animal, ISpeakable, ISelfPresentable
    {
        public void Speak()
        {
            Console.WriteLine(name+" goes Meow");
        }

        public Cat(string name) : base(name)
        { }

        public void SelfPresent()
        {
            Console.WriteLine(name +"is my name.");
        }
    }
}
